import csv, json

def convert(filename):
    with open(filename) as csvF:
        allData = []
        csvR = csv.reader(csvF, delimiter=';')
        header = None
        for row in csvR:
            if header is None:
                header = row
                continue
            data = {}
            for i, val in enumerate(row):
                h = header[i]
                if h in ['covered', 'coveredBy', 'coveredAmount']:
                    print(h)
                    data[h] = val
                else :
                    try:
                        data[h] = int(val)
                    except:
                        try:
                            data[h] = float(val)
                        except:
                            data[h] = val
            allData.append(data)
        with open(filename.replace("csv", "js"), "w") as jf:
            j = json.dumps(allData, sort_keys=True, indent=4)
            jf.write("var "+filename.replace(".csv", "")+"_data = "+j)

convert("targets.csv")
convert("viewpoints.csv")