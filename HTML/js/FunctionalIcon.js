/*
 * A div icon that accepts the html option to be a function that generates the div content.
 */
L.FunctionalIcon = L.DivIcon.extend({
	/* copied/adapted from L.DivIcon
	 */
	createIcon: function (oldIcon) {
		var div = document.createElement('div'), options = this.options;
		
		this._setIconStyles(div, 'icon');
		if (options.html !== false) {
			if (typeof(options.html) === "function"){
				var html = options.html();
				if (typeof(html) == "string") div.innerHTML = options.html;
				else div.appendChild(html);
			} else div.innerHTML = options.html;
		} else {
			div.innerHTML = '';
		}

		if (options.bgPos) {
			div.style.backgroundPosition =
			        (-options.bgPos.x) + 'px ' + (-options.bgPos.y) + 'px';
		}
		return div;
	},
});