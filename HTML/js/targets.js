/* LGPL License
 * Copyright, Lukas Treyer, Feb 2016
 */

/*
 * stores all targets by its id (allTargets[id] = target)
 */
var allTargets = {};

/*
 * sets the border color of all targets to black
 */
var allTargetsBlackStroke = function(){
	for (var id in allTargets){
		allTargets[id].symbol.attr("stroke", "black");
	}
}
/* 
 * removes the highlighting circle of all targets
 */
var removeAllTargetCircles = function(){
	for (var id in allTargets){
		var target = allTargets[id];
		if (target.circle !== undefined){
			if (target.circle.attr("stroke") != "white" && target.up)
				target.removeCircle();
			else if (!target.up) target.circle.attr("stroke", "white");
		}
			
	}
}

/* 
 * reset the initial position of all targets / put them all up
 */
var resetAllTargetUp = function(){
	for (var id in allTargets){
		if (!allTargets[id].up) allTargets[id].toggle();
	}
}

/*
 * turn all targets green
 */
var resetAllTargetColor = function(){
	for (var id in allTargets){
		allTargets[id].symbol.attr("fill", colorFromRedToGreen(1));
	}
}
/*
 * returns true if any of the selected targets is up 
 */
var anyUp = function(selection){
	for (var i = 0; i < selection.length; i++){
		if (allTargets[selection[i]].up) return true;
	}
	return false;
}

/*
 * target class T77 defining the shape used as marker; <br/>
 * uses FunctionalIcon <br/>
 * defines target up/down behavior upon shift+click, popup content upon normal click
 */
var T77 = (function(){
	var a = 30, a2 = a/2;
			
	var lineData_up = [ { "x": 7,   "y": a-9},  { "x": a-7,  "y": a-9},
	             { "x": a-7,  "y": 13}, { "x": a-12,  "y": 9},
	             { "x": 12,  "y": 9},  { "x": 7, "y": 13}];
	
	var lineData_down = [ { "x": 7,   "y": a-9},  { "x": a-7,  "y": a-9},
	   	             { "x": a-7,  "y": a-9}, { "x": a-12,  "y": a-9},
	   	             { "x": 12,  "y": a-9},  { "x": 7, "y": a-9}];
	
	var lineF = d3.svg.line()
	             .x(function(d) { return d.x; })
	             .y(function(d) { return d.y; })
	             .interpolate("linear-closed");
	
	var T77 = function(t){
		this.id = "t"+t.id;
		this.rotation = t.rotation || "0";
		this.up = true;
		
		var tg = this;
		var genSVG = function(){
			var xmlns = "http://www.w3.org/2000/svg";
			tg.svg = d3.select(document.createElementNS(xmlns,"svg"))
				.attr("id", tg.id)
				.attr("width", a)
				.attr("height", a)
				.attr("class", "target");
		
			tg.symbol = tg.svg.append("g").append("path")
		        .attr("d", lineF(lineData_up))
		        .attr("stroke", "black")
		        .attr("stroke-width", 1)
		        .attr("fill", "yellow")
		        .attr("transform", "rotate("+tg.rotation+", "+a2+", "+a2+")");
			
			return tg.svg.node();
		}
		
		this.divIcon = new L.FunctionalIcon({
		    iconSize: L.point(a,a),
		    iconAnchor: L.point(a2,a2),
		    html: genSVG
		});

		this.marker = L.marker([t.lat,t.lon],{icon:this.divIcon})
			.bindPopup("", {'maxWidth':130, 'offset':L.point(0,-100)})
			.off("click")
			.on("click", function(e){tg.click(this)});
		this.marker.target = this;

		allTargets[this.id] = this;
	} 
	
	/*
	 * SHIFT+click = toggle target up/down<br/>
	 * click without SHIFT = show a popup that tells the distance to the viewpoint
	 */
	T77.prototype.click = function(marker){
		if (shift_pressed){
			this.toggle();
		} else {
			if (currentViewPointMarker !== undefined){
				e = marker.getPopup();
				var d = currentViewPointMarker.getLatLng().distanceTo(marker.getLatLng());
				marker.getPopup().setContent("Distanz zu Punkt "
					+currentViewPointMarker.viewPoint.id.substring(1)+": <br/><b>"+Math.round(d)+"m</b>");
				marker.openPopup();
				allTargetsBlackStroke();
				this.symbol.attr("stroke", "red");
			}
		}
	}
	
	/*
	 * adds a highlight circle below the target symbol <br/>
	 * - white fill = target is down <br/>
	 * - blue stroke = target covers another target <br/>
	 * - red stroke = target is covered by another target
	 */
	T77.prototype.addCircle = function(color){
		if (this.circle == undefined){
			this.circleG = this.svg.insert("g", ":first-child");
			this.circle = this.circleG.append("circle")
				.attr("r", 12)
				.attr("cx", a2)
				.attr("cy", a2)
				.style("stroke-width", 4)
				.attr("stroke", color)
				.attr("fill", "None")
		} else {
			this.circle.attr("stroke", color);
		}
		return this.circle;
	}
	
	/*
	 * removes the circle of this target (used when a target is toggled up and it is not covered
	 * by another target and does not cover another target 
	 */
	T77.prototype.removeCircle = function(){
		if (this.circle !== undefined) {
			this.circle.remove();
			this.circleG.remove();
			this.circle = undefined;
			this.circleG = undefined;
		}
	}
	
	/*
	 * toggles (animates) between up and down state of the target and modifies the colors of
	 * the targets in case they cover / are covered
	 */
	T77.prototype.toggle = function(){
		var covers = "t0";
		var by = 0;
		var directVisibility = 1;
		if (currentViewPointMarker !== undefined){
			var vp = currentViewPointMarker.viewPoint;
			for (var ID in vp.covered){
				if (vp.covered[ID].by.indexOf(this.id) >= 0) {
					covers = ID;
					by = vp.covered[ID].amount;
				}
			}
			directVisibility = currentViewPointMarker.viewPoint.views[this.id];
		}
		
		if (this.up) {
			// DOWN
			this.symbol.transition()
				.attr("d", lineF(lineData_down))
				.attr("stroke", "red")
				.attr("stroke-width",3);
			this.up = false;
			var color = (this.circle !== undefined) ? this.circle.attr("stroke") : "white";
			this.addCircle(color).attr("fill", "white");
			if (covers != "t0") {
				allTargets[covers].symbol
					.attr("fill", colorFromRedToGreen(directVisibility))
					.attr("vAmount", directVisibility);
			}
		} else {
			// UP
			this.symbol.transition()
				.attr("d", lineF(lineData_up))
				.attr("stroke", "black")
				.attr("stroke-width",1);
			this.up = true;
			if (this.circle.attr("stroke") == "white") this.removeCircle();
			else this.circle.attr("fill", "None");
			
			if (covers != "t0") {
				allTargets[covers].symbol
					.attr("fill", colorFromRedToGreen(by))
					.attr("vAmount", by);
			}
		}
	}
	return T77;
})();