/* LGPL License
 * Copyright, Lukas Treyer, Feb 2016
 */

/*
 * stores all viewpoints by its id (allViewPoints[id] = viewPoint)
 */
var allViewPoints = {};

/*
 * the currently selected viewpoint marker
 */
var currentViewPointMarker;

/*
 * transforms a value in the interval [0.0 - 1.0] to a color between red an green using d3's hsl 
 * color representation
 */
function colorFromRedToGreen(normalized){
	return d3.hsl(normalized*100, 1.0, 0.5).toString()
}

/*
 * reset viewpoints to: radius 5, black border, fill between red and green representing average 
 * visibility
 */
var resetAllViewPoints = function(){
	for (var id in allViewPoints){
		var viewPoint = allViewPoints[id];
		viewPoint.symbol.attr("r", 5)
			.attr("stroke", "black")
			.attr("fill", colorFromRedToGreen(viewPoint.avg));
	}
}

/*
 * takes an array of strings and tries to parse each string to an int number; returns the resulting 
 * array containing presumabably only numbers
 */
var parseAllInts = function(strings){
	ints = [];
	for (var i = 0; i < strings.length; i++){
		try {
			ints.push(parseInt(strings[i]));
		} catch (e) {
			ints.push(strings[i]);
		}
	}
	return ints;
}

/*
 * takes an array of strings an adds a "t" in front of all entries
 */
var parseAllTargetIds = function(strings){
	for (var i = 0; i < strings.length; i++){
		strings[i] = "t"+strings[i];
	}
	return strings;
} 

/*
 * ViewPoint class encapsulating all (most) view point behavior <br/>
 * - defines the shape and color of viewpoints (red to green circle representing the avg visibility)<br/>
 * - onclick: highlight the viewpoint, and also the piste to which it belongs and color all targets
 * accordingly
 */
var ViewPoint = (function(){
	var a = 20, a2 = a/2;
	var nonTargetKeys = ['id','farb','piste','lat','lon','x','y','height','covered','coveredBy','coveredAmount'];

	var ViewPoint = function(v){
		this.id = "v"+v.id;
		this.piste = parseInt(v.piste);
		this.views = {};
		this.avg = 0.0;
		
		for (var k in v){
			if (nonTargetKeys.indexOf(k) < 0){
				this.avg += this.views[k] = parseFloat(v[k]);
			}
		}
		this.avg = this.avg / Object.keys(this.views).length;
		var covered = splitToArray(v.covered, ",");
		var coveredBy = splitToArray(v.coveredBy, ",");
		var coveredAmount = splitToArray(v.coveredAmount, ",");
		
		this.covered = {}
		for (var i = 0; i < covered.length; i++){
			this.covered["t"+covered[i]] = {
				by: parseAllTargetIds(coveredBy[i].split("&")),
				amount: parseFloat(coveredAmount[i]),
				id: "t"+covered[i]
			}
		}
		
		var vp = this;
		var genSVG = function(){
			var xmlns = "http://www.w3.org/2000/svg";
			vp.svg = d3.select(document.createElementNS(xmlns,"svg"))
				.attr("id", vp.id)
				.attr("width", a)
				.attr("height", a)
				.attr("class", "viewpoint");
			
			vp.symbol = vp.svg.append("g").append("circle")
				.attr('cx', a2)
			    .attr('cy', a2)
			    .attr('r', 5)
			    .style("stroke-opacity", 1)
			    .style("stroke-width", 1)
			    .attr("stroke", "black")
				.attr("fill", colorFromRedToGreen(vp.avg));
			
			return vp.svg.node();
		}
		
		this.divIcon = new L.FunctionalIcon({
		    iconSize: L.point(a,a),
		    iconAnchor: L.point(a2,a2),
		    className: "piste"+this.piste,
		    html: genSVG
		});
		
		this.marker = L.marker([v.lat,v.lon],{icon:this.divIcon})
			.on("click", function(){vp.click(this)})
			.bindPopup("<b>Piste: "+v.piste+", Punkt: "+v.id+"</b><br/><small>"
					+v.lat.toString().substring(0,6)+"N, "+v.lon.toString().substring(0,6)+"E<br/>"
					+v.x.toString().substring(0,9)+", "+v.y.toString().substring(0,9)+"<br/>"
					+"average visibility: "+vp.avg.toString().substring(0,4)+"</small", 
					{'maxWidth':130, 'offset':L.point(0,-100)});
		this.marker.viewPoint = this;
		
		allViewPoints[this.id] = this;
	}

	ViewPoint.prototype.click = function(marker){
		currentViewPointMarker = marker;
		allTargetsBlackStroke();
		removeAllTargetCircles();
		for (var ID in this.views){
			var fill = colorFromRedToGreen(this.views[ID]);
//			if (ID in this.covered && !anyUp(this.covered[ID].by)) fill = 1;
			allTargets[ID].symbol.attr("fill", fill);
			allTargets[ID].symbol.attr("vAmount", this.views[ID]);
		}
		for (var ID in this.covered){
			var covered = this.covered[ID];
			for (var i = 0; i < covered.by.length; i++){
				var cover = allTargets[covered.by[i]];
				cover.addCircle("blue");
				var fill = colorFromRedToGreen(covered.amount);
				if (fill == 1) console.log("fill == 1; covered");
				if (cover.up) allTargets[ID].symbol.attr("fill", fill);
			}
			allTargets[ID].addCircle("red");
		}
		for (var id in allViewPoints){
			var v = allViewPoints[id];
			var circle = v.symbol;
			
			if (v.piste !== this.piste){
				circle
					.attr("r", 3)
					.attr("stroke", "black")
					.attr("fill",colorFromRedToGreen(v.avg));
			} else {
				circle
					.attr("r", 5)
					.attr("fill", "white")
					.attr("stroke",colorFromRedToGreen(v.avg));
			}
		}
		this.symbol.attr("fill", "blue");
	}
	
	/*
	 * returns empty array if the given string == ""
	 */
	function splitToArray(string, delimiter){
		var array = string.split(delimiter);
		if (array[0] === "") return [];
		return array;
	}
	return ViewPoint;
})();