#Project Folder Used to Generate the Interactive Webmap of SPL Hinterrhein

##Interactive Webmap
SPL Hinterrhein (military shooting range) provides an interactive webmap that allows teachers / 
people in charge to get an overview of all the facilities of SPL Hinterrhein and its environment.
The map can be accessed offline (static html + js) and does not rely on any server infrastructure.
The orthofoto for closer zooms and the LK25 map by swisstopo are stored locally. A special overlay
concerns the visibility of the TAA77 targets. It allows to explore the visibility of the TAA77 targets
in an interactive way for selected, precalculated ~50 viewpoints on the tank piste. It also allows
to show and hide individual targets to explore the visibility in case targets cover each other. 

![SPL Hinterrhein](SPLHinterrhein.jpg)  
_Screenshot of the webmap_  

The hereby shared project folder does not contain any map content but the QGIS file necessary to 
generate the tiles for the webmap. For a description on how to do so refer to the QGIS section below.

**Folder Structure**:

* HTML contains the webmap. Tiles need to be exported to a folder HTML/_tiles.
* SPL.qgs: QGIS file referring to all shapefiles that also 

**Contact/Author**:
This webmap was created during military service 2016 by Lukas Treyer, Zurich.

##QGIS

###QMetaTiles
[QGIS](http://qgis.org/de/site/) comes with an extension [QMetaTiles](http://gis-lab.info/qa/qtiles-eng.html) 
that can be installed from the extensions repository right 
within QGIS. It allows to export the active QGIS layers to tiles - either in mbtiles format 
(sqlite database) or plain png/jpg files in a regular folder structure. It also features an optional
html viewer that is able to display the exported tiles as a webmap. This html viewer 
(a simple .html file) can be used as a starting point / can be adapted to your needs. This very 
project represents such an adaption.

**Enable Meta Tiles Option**:
The QMetaTiles allows to not only export normal tiles but also meta tiles that are larger than normal
tiles. This can be very helpful in case you export a layer with labels, because it avoids repeated
labels for the same item.

###Layer Options
For certain raster layers a resampling is needed to create nice looking tiles. To do so double click 
on the layer in the Layers Panel and in the Style category under resampling change the "zoomed in"
setting to cubic, "zoomed out" to average and oversampling to 4.00.

###Symbols based on columns
**Rotation**: To rotate a symbol you can add a column to the attributes table that stores the 
rotation of a symbol and link it in the styles editor to the symbol as illustrated below: To make the
rotation parametric link it to the attribute field (column) in your table.
![Symbol Rotation](rotation.png)
**Symbol Type**: To make the symbol type dependent on a column QGIS allows to display symbols in 
categorized manner, where the category of symbols can be linked to columns. QGIS allows to represent 
symbols with SVG files. Once you store a symbol definition it can be selected in the category view.
![Symbol Categories](symbol_categories.png)